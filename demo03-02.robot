*** Settings ***
Library  SeleniumLibrary
Test Teardown  Close Browser

*** Variables ***
${web_browser}    chrome
#${web_browser}    firefox
#${web_browser}    edge
${url_manage}       http://127.0.0.1:8080/manageStudentConsider
${btnSearch}                    btnsearch
${pic_add}                      xpath=//a[@href="/manageStudentCon_add"]
${pic_edit}                     xpath=//a[@href="/manageCon_edit/1"]
${pic_delete}                   xpath=//a[@href="/delCon/1"]
${btnCancel}                    cancel
${txt_searchuser}               searchuser
${firstname}                    นาย ชรัญธร
${lastname}                     วงษ์ไทย
${stuid}                        58660044

*** Keywords ***
เปิดหน้าจอ
    [Arguments]             ${url}          
    [Documentation]         กดลิ้งค์เพื่อเปิด Browser
    Open Browser            ${url}             ${web_browser}
กรอกข้อมูล
    [Arguments]             ${txt}             ${data_search}
    [Documentation]         กรอกข้อมูล
    Input Text              ${txt}             ${data_search}
กดปุ่ม
    [Arguments]             ${btn}            
    [Documentation]         กดปุ่ม
    Click Button            ${btn}
กดลิ้งค์
    [Arguments]             ${link}            
    [Documentation]         กดลิ้งค์
    Click Link              ${link}
*** Test Cases ***
TC-PSF-03-02-01 ตรวจสอบการกดปุ่มค้นหา
    เปิดหน้าจอ        ${url_manage}
    กดปุ่ม            ${btnSearch}
TC-PSF-03-02-02 ตรวจสอบการกดปุ่มเพิ่มข้อมูล
    เปิดหน้าจอ        ${url_manage}
    กดลิ้งค์            ${pic_add} 
TC-PSF-03-02-03 ตรวจสอบการกดปุ่มแก้ไข
    เปิดหน้าจอ        ${url_manage}
    กดลิ้งค์            ${pic_edit}
TC-PSF-03-02-04 ตรวจสอบการกดปุ่มลบ
    เปิดหน้าจอ        ${url_manage}
    กดลิ้งค์            ${pic_delete}



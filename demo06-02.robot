*** Settings ***
Library  SeleniumLibrary
Test Teardown  Close Browser

*** Variables ***
${web_browser}    chrome
#${web_browser}    firefox
#${web_browser}    edge
${url_manage}       http://127.0.0.1:8080/followStudentEnp
${btnSearch}                    btnsearch
${pic_follow}                   xpath=//a[@href="/followStudentEnpReg/4"]
${txt_searchuser}               searchuser

*** Keywords ***
เปิดหน้าจอ
    [Arguments]             ${url}          
    [Documentation]         กดลิ้งค์เพื่อเปิด Browser
    Open Browser            ${url}             ${web_browser}
กรอกข้อมูล
    [Arguments]             ${txt}             ${data_search}
    [Documentation]         กรอกข้อมูล
    Input Text              ${txt}             ${data_search}
กดปุ่ม
    [Arguments]             ${btn}            
    [Documentation]         กดปุ่ม
    Click Button            ${btn}
กดลิ้งค์
    [Arguments]             ${link}            
    [Documentation]         กดลิ้งค์
    Click Link              ${link}
*** Test Cases ***
TC-PSF-06-02-01 ตรวจสอบการกดปุ่มค้นหา
    เปิดหน้าจอ        ${url_manage}
    กดปุ่ม            ${btnSearch}
TC-PSF-06-02-02 ตรวจสอบการกดปุ่มติดตาม
    เปิดหน้าจอ        ${url_manage}
    กดลิ้งค์            ${pic_follow}




*** Settings ***
Library  SeleniumLibrary
Test Teardown  Close Browser

*** Variables ***
${web_browser}    chrome
#${web_browser}    firefox
#${web_browser}    edge
${url_manage}       http://127.0.0.1:8080/manageStudentConsider
${btnSearch}                    btnsearch
${pic_add}                      xpath=//a[@href="/manageStudentCon_add"]
${pic_edit}                     xpath=//a[@href="/manageCon_edit/1"]
${pic_delete}                   xpath=//a[@href="/delCon/5"]
${txt_studenid}                 studentIDCon
${txt_studnameandlast}          nameStuCon
${btnok}                        ok
${btncancel}                    cancel

*** Keywords ***
เปิดหน้าจอ
    [Arguments]             ${url}          
    [Documentation]         กดลิ้งค์เพื่อเปิด Browser
    Open Browser            ${url}             ${web_browser}
กรอกข้อมูล
    [Arguments]             ${txt}             ${input}
    [Documentation]         กรอกข้อมูล
    Input Text              ${txt}             ${input}
กดปุ่ม
    [Arguments]             ${btn}            
    [Documentation]         กดปุ่ม
    Click Button            ${btn}
กดลิ้งค์
    [Arguments]             ${link}            
    [Documentation]         กดลิ้งค์
    Click Link              ${link}
    
*** Test Cases ***
TC-PSF-03-11-01 ตรวจสอบการกดปุ่มยกเลิก
    เปิดหน้าจอ        ${url_manage}
    กดลิ้งค์           ${pic_delete} 
    กดปุ่ม            ${btncancel}
TC-PSF-03-11-02 ตรวจสอบการกดปุ่มตกลง
    เปิดหน้าจอ        ${url_manage}
    กดลิ้งค์           ${pic_delete} 
    กดปุ่ม            ${btnok} 





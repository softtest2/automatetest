*** Settings ***
Library  SeleniumLibrary
Test Teardown  Close Browser

*** Variables ***
${web_browser}    chrome
#${web_browser}    firefox
#${web_browser}    edge
${url_manage}       http://127.0.0.1:8080/manageStudentConsider
${btnSearch}                    btnsearch
${pic_add}                      xpath=//a[@href="/manageStudentCon_add"]
${pic_edit}                     xpath=//a[@href="/manageCon_edit/1"]
${pic_delete}                   xpath=//a[@href="/delCon/1"]
${txt_studenid}                 studentIDCon
${txt_studnameandlast}          nameStuCon
${txt_grade}                    stuGpaCon
${txt_gradepass}                creditPassedCon
${txt_gradedown}                creditDownCon
${dropdownlevel}                stuLevelCon
${dropdownstatus}               stuStatusCon
${dropdownconsider}             considerCon
${dropdowncount}                considertime

*** Keywords ***
เปิดหน้าจอ
    [Arguments]             ${url}          
    [Documentation]         กดลิ้งค์เพื่อเปิด Browser
    Open Browser            ${url}             ${web_browser}
กรอกข้อมูล
    [Arguments]             ${txt}             ${input}
    [Documentation]         กรอกข้อมูล
    Input Text              ${txt}             ${input}
กดลิ้งค์
    [Arguments]             ${link}            
    [Documentation]         กดลิ้งค์
    Click Link              ${link}
เลือกลิสต์เลเวล
    # [Arguments]                 ${list}
    [Documentation]             เลือกจากในลิสต์
    Get List Items              ${dropdownlevel}           
เลือกลิสต์สถานะ
    # [Arguments]                 ${list}
    [Documentation]             เลือกจากในลิสต์
    Get List Items              ${dropdownstatus}          
เลือกลิสต์พิจารณา
    # [Arguments]                 ${list}
    [Documentation]             เลือกจากในลิสต์
    Get List Items              ${dropdownconsider}        
เลือกลิสต์จำนวน
    # [Arguments]                 ${list}
    [Documentation]             เลือกจากในลิสต์
    Get List Items              ${dropdowncount}           
*** Test Cases ***
TC-PSF-03-10-01 ตรวจสอบการแสดงผลฟิลเตอร์ Dropdown ข้อมูลระดับ
    เปิดหน้าจอ        ${url_manage}
    กดลิ้งค์           ${pic_edit}
    เลือกลิสต์เลเวล     
TC-PSF-03-10-02 ตรวจสอบการแสดงผลฟิลเตอร์ Dropdown ข้อมูลสถานภาพ
    เปิดหน้าจอ        ${url_manage}
    กดลิ้งค์           ${pic_edit}
    เลือกลิสต์สถานะ    
TC-PSF-03-10-03 ตรวจสอบการแสดงผลฟิลเตอร์ Dropdown ข้อมูลครั้งที่
    เปิดหน้าจอ        ${url_manage}
    กดลิ้งค์           ${pic_edit}
    เลือกลิสต์พิจารณา   
TC-PSF-03-10-04 ตรวจสอบการแสดงผลฟิลเตอร์ Dropdown ข้อมูลรอการพิจารณา
    เปิดหน้าจอ        ${url_manage}
    กดลิ้งค์           ${pic_edit}
    เลือกลิสต์จำนวน     






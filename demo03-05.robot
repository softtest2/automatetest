*** Settings ***
Library  SeleniumLibrary
Test Teardown  Close Browser

*** Variables ***
${web_browser}    chrome
#${web_browser}    firefox
#${web_browser}    edge
${url_manage}       http://127.0.0.1:8080/manageStudentConsider
${btnSearch}                    btnsearch
${pic_add}                      xpath=//a[@href="/manageStudentCon_add"]
${pic_edit}                     xpath=//a[@href="/manageCon_edit/1"]
${pic_delete}                   xpath=//a[@href="/delCon/1"]
${txt_studenid}                 studentIDCon
${txt_studnameandlast}          nameStuCon
${txt_grade}                    stuGpaCon
${txt_gradepass}                creditPassedCon
${txt_gradedown}                creditDownCon
${dropdownlevel}                stuLevelCon
${dropdownstatus}               stuStatusCon
${dropdownconsider}             considerCon
${dropdowncount}                considertime
${nameandlast3}                 พีชญุตม์ ธนะประสพ
${stuid3}                       63160210
${grade3}                       1.79
${credit2}                      108
${credit3}                      111
${list1}                        ตรี ปกติ
${list2}                        10
${list3}                        โปรต่ำ
${list4}                        1
${btnok}                        ok
${btncancel}                    cancel

*** Keywords ***
เปิดหน้าจอ
    [Arguments]             ${url}          
    [Documentation]         กดลิ้งค์เพื่อเปิด Browser
    Open Browser            ${url}             ${web_browser}
กรอกข้อมูล
    [Arguments]             ${txt}             ${input}
    [Documentation]         กรอกข้อมูล
    Input Text              ${txt}             ${input}
กดปุ่ม
    [Arguments]             ${btn}            
    [Documentation]         กดปุ่ม
    Click Button            ${btn}
กดลิ้งค์
    [Arguments]             ${link}            
    [Documentation]         กดลิ้งค์
    Click Link              ${link}
เลือกลิสต์เลเวล
    [Arguments]                 ${list}
    [Documentation]             เลือกจากในลิสต์
    Select From List By Label   ${dropdownlevel}           ${list}
เลือกลิสต์สถานะ
    [Arguments]                 ${list}
    [Documentation]             เลือกจากในลิสต์
    Select From List By Label   ${dropdownstatus}          ${list}
เลือกลิสต์พิจารณา
    [Arguments]                 ${list}
    [Documentation]             เลือกจากในลิสต์
    Select From List By Label   ${dropdownconsider}        ${list}
เลือกลิสต์จำนวน
    [Arguments]                 ${list}
    [Documentation]             เลือกจากในลิสต์
    Select From List By Label   ${dropdowncount}           ${list}
สลับไปหน้าจอใหม่
    [Documentation]             ใช้สลับไปหน้าจอใหม่
    switch Window               locator=NEW
*** Test Cases ***
TC-PSF-03-05-01 ตรวจสอบการกดปุ่มบันทึก
    เปิดหน้าจอ        ${url_manage}
    กดลิ้งค์           ${pic_add}
    สลับไปหน้าจอใหม่ 
    กรอกข้อมูล        ${txt_studenid}                 ${stuid3}
    กรอกข้อมูล        ${txt_studnameandlast}         ${nameandlast3}
    เลือกลิสต์เลเวล     ${list1}
    กรอกข้อมูล        ${txt_grade}                   ${grade3}     
    เลือกลิสต์สถานะ    ${list2}
    กรอกข้อมูล        ${txt_gradepass}               ${credit2}
    กรอกข้อมูล        ${txt_gradedown}               ${credit3}
    เลือกลิสต์พิจารณา   ${list3}
    เลือกลิสต์จำนวน    ${list4}
    กดปุ่ม            ${btnok}
TC-PSF-03-05-02 ตรวจสอบการกดยกเลิก
    เปิดหน้าจอ        ${url_manage}
    กดลิ้งค์           ${pic_add}
    สลับไปหน้าจอใหม่ 
    กรอกข้อมูล        ${txt_studenid}                 ${stuid3}
    กดปุ่ม            ${btncancel}






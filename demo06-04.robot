*** Settings ***
Library  SeleniumLibrary
Test Teardown  Close Browser

*** Variables ***
${web_browser}    chrome
#${web_browser}    firefox
#${web_browser}    edge
${url_manage}       http://127.0.0.1:8080/followStudentEnp
${btnSearch}                    btnsearch
${pic_follow}                   xpath=//a[@href="/followStudentEnpReg/4"]
${txt_stuEnpReg}                stuEnpReg
${input}                        นิสิตลงวิชาเรียนใหม่หรือยังครับ
${input1}                       ${EMPTY}


*** Keywords ***
เปิดหน้าจอ
    [Arguments]             ${url}          
    [Documentation]         กดลิ้งค์เพื่อเปิด Browser
    Open Browser            ${url}             ${web_browser}
กรอกข้อมูล
    [Arguments]             ${txt}             ${data_search}
    [Documentation]         กรอกข้อมูล
    Input Text              ${txt}             ${data_search}
กดปุ่ม
    [Arguments]             ${btn}            
    [Documentation]         กดปุ่ม
    Click Button            ${btn}
กดลิ้งค์
    [Arguments]             ${link}            
    [Documentation]         กดลิ้งค์
    Click Link              ${link}
*** Test Cases ***
TC-PSF-06-04-01 ตรวจสอบการกรอกข้อมูลแผนการเรียน
    เปิดหน้าจอ        ${url_manage}
    กดลิ้งค์            ${pic_follow}
    กรอกข้อมูล        ${txt_stuEnpReg}        ${input}
TC-PSF-06-04-02 ตรวจสอบการกรอกข้อมูลแผนการเรียน หากไม่กรอกข้อมูล
    เปิดหน้าจอ        ${url_manage}
    กดลิ้งค์            ${pic_follow}
    กรอกข้อมูล        ${txt_stuEnpReg}        ${input1}




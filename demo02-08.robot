*** Settings ***
Library  SeleniumLibrary
Test Teardown  Close Browser

*** Variables ***
${web_browser}    chrome
#${web_browser}    firefox
#${web_browser}    edge
${url_manage}       http://127.0.0.1:8080/manageUser
${pic_edit}         xpath=//a[@href="/manageUser_edit/15"]
${btnSave}          ok
${btnCancel}        cancel
${txt_name}       nameuser

*** Keywords ***
เปิดหน้าจอ
    [Arguments]             ${url}          
    [Documentation]         กดลิ้งค์เพื่อเปิด Browser
    Open Browser            ${url}             ${web_browser}
กรอกข้อมูล
    [Arguments]             ${txt}             ${data_search}
    [Documentation]         กรอกข้อมูล
    Input Text              ${txt}             ${data_search}
กดปุ่ม
    [Arguments]             ${btn}            
    [Documentation]         กดปุ่ม
    Click Button            ${btn}
กดลิ้งค์
    [Arguments]             ${link}            
    [Documentation]         กดลิ้งค์
    Click Link            ${link}

*** Test Cases ***
TC-PSF-02-08-01 ตรวจสอบการกดปุ่มบันทึก
    เปิดหน้าจอ        ${url_manage}
    กดลิ้งค์           ${pic_edit}
    กรอกข้อมูล        ${txt_name}         ประยวยหัว
    กดปุ่ม            ${btnSave}
TC-PSF-02-08-02 ตรวจสอบการกดปุ่มยกเลิก
    เปิดหน้าจอ        ${url_manage}
    กดลิ้งค์           ${pic_edit}
    กดปุ่ม            ${btnCancel}

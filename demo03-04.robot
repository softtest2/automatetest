*** Settings ***
Library  SeleniumLibrary
Test Teardown  Close Browser

*** Variables ***
${web_browser}    chrome
#${web_browser}    firefox
#${web_browser}    edge
${url_manage}       http://127.0.0.1:8080/manageStudentConsider
${btnSearch}                    btnsearch
${pic_add}                      xpath=//a[@href="/manageStudentCon_add"]
${pic_edit}                     xpath=//a[@href="/manageCon_edit/1"]
${pic_delete}                   xpath=//a[@href="/delCon/1"]
${txt_studenid}                 studentIDCon
${txt_studnameandlast}          nameStuCon
${txt_grade}                    stuGpaCon
${txt_gradepass}                creditPassedCon
${txt_gradedown}                creditDownCon
${dropdownlevel}                stuLevelCon
${dropdownstatus}               stuStatusCon
${dropdownconsider}             considerCon
${dropdowncount}                considertime
${nameandlast}                  พีชญุตม์ ธนะประสพ12
${nameandlast3}                 พีชญุตม์ ธนะประสพ
${nameandlast2}                 กฤษณ์ดนัย ศรีรักษา22222222222222222222222222222222
${stuid}                        58660044
${stuid2}                       63160
${stuid3}                       63160210
${grade}                        2.10สอง
${grade2}                       1.779
${grade3}                       1.79
${credit}                       1.5
${credit2}                      108
${credit3}                      111
${gradepass}                    142สอง
${gradedown}                    150ศูนย์
${list1}                        ตรี ปกติ
${list2}                        10
${list3}                        โปรต่ำ
${list4}                        1


*** Keywords ***
เปิดหน้าจอ
    [Arguments]             ${url}          
    [Documentation]         กดลิ้งค์เพื่อเปิด Browser
    Open Browser            ${url}             ${web_browser}
กรอกข้อมูล
    [Arguments]             ${txt}             ${input}
    [Documentation]         กรอกข้อมูล
    Input Text              ${txt}             ${input}
กดปุ่ม
    [Arguments]             ${btn}            
    [Documentation]         กดปุ่ม
    Click Button            ${btn}
กดลิ้งค์
    [Arguments]             ${link}            
    [Documentation]         กดลิ้งค์
    Click Link              ${link}
เลือกลิสต์เลเวล
    [Arguments]                 ${list}
    [Documentation]             เลือกจากในลิสต์
    Select From List By Label   ${dropdownlevel}           ${list}
เลือกลิสต์สถานะ
    [Arguments]                 ${list}
    [Documentation]             เลือกจากในลิสต์
    Select From List By Label   ${dropdownstatus}          ${list}
เลือกลิสต์พิจารณา
    [Arguments]                 ${list}
    [Documentation]             เลือกจากในลิสต์
    Select From List By Label   ${dropdownconsider}        ${list}
เลือกลิสต์จำนวน
    [Arguments]                 ${list}
    [Documentation]             เลือกจากในลิสต์
    Select From List By Label   ${dropdowncount}           ${list}
สลับไปหน้าจอใหม่
    [Documentation]             ใช้สลับไปหน้าจอใหม่
    switch Window               locator=NEW
    
*** Test Cases ***
TC-PSF-03-04-01 ตรวจสอบการกรอกข้อมูลรหัสนิสิต
    เปิดหน้าจอ        ${url_manage}
    กดลิ้งค์           ${pic_add} 
    สลับไปหน้าจอใหม่
    กรอกข้อมูล        ${txt_studenid}                 ${stuid}
TC-PSF-03-04-02 ตรวจสอบการกรอกข้อมูล ชื่อ-นามสกุล
    เปิดหน้าจอ        ${url_manage}
    กดลิ้งค์           ${pic_add} 
    สลับไปหน้าจอใหม่
    กรอกข้อมูล        ${txt_studnameandlast}         ${nameandlast}
TC-PSF-03-04-03 ตรวจสอบการกรอกข้อมูลเกรดเฉลี่ย
    เปิดหน้าจอ        ${url_manage}
    กดลิ้งค์           ${pic_add} 
    สลับไปหน้าจอใหม่
    กรอกข้อมูล        ${txt_grade}                   ${grade}
TC-PSF-03-04-04 ตรวจสอบการกรอกข้อมูลหน่วยกิตที่ผ่าน
    เปิดหน้าจอ        ${url_manage}
    กดลิ้งค์           ${pic_add} 
    สลับไปหน้าจอใหม่
    กรอกข้อมูล        ${txt_gradepass}               ${gradepass}
TC-PSF-03-04-05 ตรวจสอบการกรอกข้อมูลหน่วยกิตที่ลง
    เปิดหน้าจอ        ${url_manage}
    กดลิ้งค์           ${pic_add} 
    สลับไปหน้าจอใหม่
    กรอกข้อมูล        ${txt_gradedown}               ${gradedown}
TC-PSF-03-04-06 ตรวจสอบการกรอกข้อมูลรหัสนิสิต รับไม่เกิน 8 ตัวอักษร และไม่น้อยกว่า 8 ตัวอักษร
    เปิดหน้าจอ        ${url_manage}
    กดลิ้งค์           ${pic_add} 
    สลับไปหน้าจอใหม่
    กรอกข้อมูล        ${txt_studenid}                ${stuid2}
TC-PSF-03-04-07 ตรวจสอบการกรอกข้อมูลชื่อ - นามสกุล รับไม่เกิน 50 ตัวอักษร
    เปิดหน้าจอ        ${url_manage}
    กดลิ้งค์           ${pic_add} 
    สลับไปหน้าจอใหม่
    กรอกข้อมูล        ${txt_studnameandlast}         ${nameandlast2}
TC-PSF-03-04-08 ตรวจสอบการกรอกข้อมูลเกรดเฉลี่ยด้วยเลขทศนิยมมากว่า 2 ตำแหน่ง
    เปิดหน้าจอ        ${url_manage}
    กดลิ้งค์           ${pic_add} 
    สลับไปหน้าจอใหม่
    กรอกข้อมูล        ${txt_grade}                   ${grade2}
TC-PSF-03-04-09 ตรวจสอบการกรอกข้อมูลหน่วยกิตที่ผ่านด้วยเลขทศนิยม
    เปิดหน้าจอ        ${url_manage}
    กดลิ้งค์           ${pic_add} 
    สลับไปหน้าจอใหม่
    กรอกข้อมูล        ${txt_gradepass}               ${credit}
TC-PSF-03-04-10 ตรวจสอบการกรอกข้อมูลหน่วยกิตที่ลงด้วยเลขทศนิยม
    เปิดหน้าจอ        ${url_manage}
    กดลิ้งค์           ${pic_add} 
    สลับไปหน้าจอใหม่
    กรอกข้อมูล        ${txt_gradedown}               ${credit}
TC-PSF-03-04-11 ตรวจสอบการหากไม่กรอกข้อมูลรหัสนิสิต
    เปิดหน้าจอ        ${url_manage}
    กดลิ้งค์           ${pic_add}
    สลับไปหน้าจอใหม่
    กรอกข้อมูล        ${txt_studnameandlast}         ${nameandlast3}
    เลือกลิสต์เลเวล     ${list1}
    กรอกข้อมูล        ${txt_grade}                   ${grade3}     
    เลือกลิสต์สถานะ    ${list2}
    กรอกข้อมูล        ${txt_gradepass}               ${credit2}
    กรอกข้อมูล        ${txt_gradedown}               ${credit3}
    เลือกลิสต์พิจารณา   ${list3}
    เลือกลิสต์จำนวน    ${list4}
TC-PSF-03-04-12 ตรวจสอบการหากไม่กรอกข้อมูลชื่อ-นามสกุล
    เปิดหน้าจอ        ${url_manage}
    กดลิ้งค์           ${pic_add}
    สลับไปหน้าจอใหม่
    กรอกข้อมูล        ${txt_studenid}                ${stuid3}
    เลือกลิสต์เลเวล     ${list1}
    กรอกข้อมูล        ${txt_grade}                   ${grade3}     
    เลือกลิสต์สถานะ    ${list2}
    กรอกข้อมูล        ${txt_gradepass}               ${credit2}
    กรอกข้อมูล        ${txt_gradedown}               ${credit3}
    เลือกลิสต์พิจารณา   ${list3}
    เลือกลิสต์จำนวน    ${list4}
TC-PSF-03-04-13 ตรวจสอบการหากไม่เลือก Dropdown ระดับ
    เปิดหน้าจอ        ${url_manage}
    กดลิ้งค์           ${pic_add}
    สลับไปหน้าจอใหม่
    กรอกข้อมูล        ${txt_studenid}                ${stuid3}
    กรอกข้อมูล        ${txt_studnameandlast}         ${nameandlast3}
    กรอกข้อมูล        ${txt_grade}                   ${grade3}     
    เลือกลิสต์สถานะ    ${list2}
    กรอกข้อมูล        ${txt_gradepass}               ${credit2}
    กรอกข้อมูล        ${txt_gradedown}               ${credit3}
    เลือกลิสต์พิจารณา   ${list3}
    เลือกลิสต์จำนวน    ${list4}
TC-PSF-03-04-14 ตรวจสอบการหากไม่กรอกข้อมูลเกรดเฉลี่ย
    เปิดหน้าจอ        ${url_manage}
    กดลิ้งค์           ${pic_add}
    สลับไปหน้าจอใหม่
    กรอกข้อมูล        ${txt_studenid}                ${stuid3}
    กรอกข้อมูล        ${txt_studnameandlast}         ${nameandlast3}
    เลือกลิสต์เลเวล     ${list1}
    เลือกลิสต์สถานะ    ${list2}
    กรอกข้อมูล        ${txt_gradepass}               ${credit2}
    กรอกข้อมูล        ${txt_gradedown}               ${credit3}
    เลือกลิสต์พิจารณา   ${list3}
    เลือกลิสต์จำนวน    ${list4}
TC-PSF-03-04-15 ตรวจสอบการหากไม่เลือก Dropdown สถานภาพ
    เปิดหน้าจอ        ${url_manage}
    กดลิ้งค์           ${pic_add}
    สลับไปหน้าจอใหม่
    กรอกข้อมูล        ${txt_studenid}                ${stuid3}
    กรอกข้อมูล        ${txt_studnameandlast}         ${nameandlast3}
    เลือกลิสต์เลเวล     ${list1}
    กรอกข้อมูล        ${txt_grade}                   ${grade3} 
    กรอกข้อมูล        ${txt_gradepass}               ${credit2}
    กรอกข้อมูล        ${txt_gradedown}               ${credit3}
    เลือกลิสต์พิจารณา   ${list3}
    เลือกลิสต์จำนวน    ${list4}
TC-PSF-03-04-16 ตรวจสอบการหากไม่กรอกข้อมูลหน่วยกิตที่ผ่าน
    เปิดหน้าจอ        ${url_manage}
    กดลิ้งค์           ${pic_add}
    สลับไปหน้าจอใหม่
    กรอกข้อมูล        ${txt_studenid}                ${stuid3}
    กรอกข้อมูล        ${txt_studnameandlast}         ${nameandlast3}
    เลือกลิสต์เลเวล     ${list1}
    กรอกข้อมูล        ${txt_grade}                   ${grade3} 
    เลือกลิสต์สถานะ    ${list2}
    กรอกข้อมูล        ${txt_gradedown}               ${credit3}
    เลือกลิสต์พิจารณา   ${list3}
    เลือกลิสต์จำนวน    ${list4}
TC-PSF-03-04-17 ตรวจสอบการหากไม่กรอกข้อมูลหน่วยกิตที่ลง
    เปิดหน้าจอ        ${url_manage}
    กดลิ้งค์           ${pic_add}
    สลับไปหน้าจอใหม่
    กรอกข้อมูล        ${txt_studenid}                ${stuid3}
    กรอกข้อมูล        ${txt_studnameandlast}         ${nameandlast3}
    เลือกลิสต์เลเวล     ${list1}
    กรอกข้อมูล        ${txt_grade}                   ${grade3} 
    เลือกลิสต์สถานะ    ${list2}
    กรอกข้อมูล        ${txt_gradepass}               ${credit2}
    เลือกลิสต์พิจารณา   ${list3}
    เลือกลิสต์จำนวน    ${list4}
TC-PSF-03-04-18 ตรวจสอบการหากไม่เลือก Dropdown รอการพิจารณา
    เปิดหน้าจอ        ${url_manage}
    กดลิ้งค์           ${pic_add}
    สลับไปหน้าจอใหม่
    กรอกข้อมูล        ${txt_studenid}                ${stuid3}
    กรอกข้อมูล        ${txt_studnameandlast}         ${nameandlast3}
    เลือกลิสต์เลเวล     ${list1}
    กรอกข้อมูล        ${txt_grade}                   ${grade3} 
    เลือกลิสต์สถานะ    ${list2}
    กรอกข้อมูล        ${txt_gradepass}               ${credit2}
    กรอกข้อมูล        ${txt_gradedown}               ${credit3}
    เลือกลิสต์จำนวน    ${list4}
TC-PSF-03-04-19 ตรวจสอบการหากไม่เลือก Dropdown ครั้งที่
    เปิดหน้าจอ        ${url_manage}
    กดลิ้งค์           ${pic_add}
    สลับไปหน้าจอใหม่
    กรอกข้อมูล        ${txt_studenid}                ${stuid3}
    กรอกข้อมูล        ${txt_studnameandlast}         ${nameandlast3}
    เลือกลิสต์เลเวล     ${list1}
    กรอกข้อมูล        ${txt_grade}                   ${grade3} 
    เลือกลิสต์สถานะ    ${list2}
    กรอกข้อมูล        ${txt_gradepass}               ${credit2}
    กรอกข้อมูล        ${txt_gradedown}               ${credit3}
    เลือกลิสต์พิจารณา   ${list3}






*** Settings ***
Library  SeleniumLibrary
Test Teardown  Close Browser

*** Variables ***
${web_browser}    chrome
#${web_browser}    firefox
#${web_browser}    edge
${url}              http://127.0.0.1:8080/
${url_manage}       http://127.0.0.1:8080/followStudentEnp
${txt_email}      email
${txt_pass}       password
${pos}            position
${btn_ok}         btnok
${user1}     pichet
${pass1}     123456
${teacher}   อาจารย์
${user2}     somnuk
${pass2}     123456
${staff}     เจ้าหน้าที่

*** Keywords ***
เปิดหน้าจอ
    [Arguments]             ${url}          
    [Documentation]         กดลิ้งค์เพื่อเปิด Browser
    Open Browser            ${url}             ${web_browser}
กรอกข้อมูล
    [Arguments]             ${txt}             ${data_search}
    [Documentation]         กรอกข้อมูล
    Input Text              ${txt}             ${data_search}
กดปุ่ม
    [Arguments]             ${btn}            
    [Documentation]         กดปุ่ม
    Click Button            ${btn}
ตรวจสอบสถานะ
    [Arguments]            ${position}
    [Documentation]        ตรวจสอบสถานะ
    Element Should Contain      ${pos}       ${position}
*** Test Cases ***
TC-PSF-06-03-01 ตรวจสอบการแสดงผลข้อมูลรายชื่อผู้ใช้งานสถานะอาจารย์
    เปิดหน้าจอ        ${url} 
    กรอกข้อมูล        ${txt_email}       ${user1}
    กรอกข้อมูล        ${txt_pass}        ${pass1}
    กดปุ่ม            ${btn_ok}
    ตรวจสอบสถานะ    ${teacher}
TC-PSF-06-03-02 ตรวจสอบการแสดงผลข้อมูลรายชื่อผู้ใช้งานสถานะเจ้าหน้าที่
    เปิดหน้าจอ        ${url} 
    กรอกข้อมูล        ${txt_email}       ${user2}
    กรอกข้อมูล        ${txt_pass}        ${pass2}
    กดปุ่ม            ${btn_ok}
    ตรวจสอบสถานะ    ${staff}



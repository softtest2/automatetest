*** Settings ***
Library  SeleniumLibrary
Test Teardown  Close Browser

*** Variables ***
${web_browser}    chrome
#${web_browser}    firefox
#${web_browser}    edge
${url_manage}       http://127.0.0.1:8080/manageStudentConsider
${btnSearch}                    btnsearch
${btnCancel}                    cancel
${txt_searchuser}               searchuser
${firstname}                    นาย ชรัญธร
${lastname}                     วงษ์ไทย
${name}                         นามสกุลลลลลลลลลลลลลลลลลลลลลลลลลลลลลลลลลลลลลลลลลลลลลล
${stuid}                        58660044

*** Keywords ***
เปิดหน้าจอ
    [Arguments]             ${url}          
    [Documentation]         กดลิ้งค์เพื่อเปิด Browser
    Open Browser            ${url}             ${web_browser}
กรอกข้อมูล
    [Arguments]             ${txt}             ${data_search}
    [Documentation]         กรอกข้อมูล
    Input Text              ${txt}             ${data_search}
กดปุ่ม
    [Arguments]             ${btn}            
    [Documentation]         กดปุ่ม
    Click Button            ${btn}

*** Test Cases ***
TC-PSF-03-01-01 ตรวจสอบการกรอกข้อมูลช่องค้นหาจากชื่อ
    เปิดหน้าจอ        ${url_manage}
    กรอกข้อมูล        ${txt_searchuser}         ${firstname}
    กดปุ่ม            ${btnSearch}
TC-PSF-03-01-02 ตรวจสอบการกรอกข้อมูลช่องค้นหาจากนาลสกุล
    เปิดหน้าจอ        ${url_manage}
    กรอกข้อมูล        ${txt_searchuser}         ${lastname}
    กดปุ่ม            ${btnSearch}
TC-PSF-03-01-03 ตรวจสอบการกรอกข้อมูลช่องค้นหาจากรหัสนิสิต
    เปิดหน้าจอ        ${url_manage}
    กรอกข้อมูล        ${txt_searchuser}         ${stuid}
    กดปุ่ม            ${btnSearch}
TC-PSF-03-01-04 ตรวจสอบการกรอกข้อมูลค้นหา รับไม่เกิน 50 ตัวอักษร
    เปิดหน้าจอ        ${url_manage}
    กรอกข้อมูล        ${txt_searchuser}         ${name}
    กดปุ่ม            ${btnSearch}


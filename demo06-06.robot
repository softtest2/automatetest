*** Settings ***
Library  SeleniumLibrary
Test Teardown  Close Browser

*** Variables ***
${web_browser}    chrome
#${web_browser}    firefox
#${web_browser}    edge
${url_manage}       http://127.0.0.1:8080/followStudentEnp
${btnSearch}                    btnsearch
${pic_follow}                   xpath=//a[@href="/followStudentEnpReg/4"]
${txt_stuEnpReg}                stuEnpReg
${btn_ok}                       ok
${btn_delete}                   btndelete
${input}                        นิสิตลงวิชาเรียนใหม่หรือยังครับ
${input1}                       ${EMPTY}

*** Keywords ***
เปิดหน้าจอ
    [Arguments]             ${url}          
    [Documentation]         กดลิ้งค์เพื่อเปิด Browser
    Open Browser            ${url}             ${web_browser}
กรอกข้อมูล
    [Arguments]             ${txt}             ${data_search}
    [Documentation]         กรอกข้อมูล
    Input Text              ${txt}             ${data_search}
กดลิ้งค์
    [Arguments]             ${link}            
    [Documentation]         กดลิ้งค์
    Click Link              ${link}
*** Test Cases ***
TC-PSF-06-06-01 ตรวจสอบการแสดงผลข้อมูลรหัสนิสิตที่เลือกจากตาราง
    เปิดหน้าจอ              ${url_manage}
    กดลิ้งค์                 ${pic_follow}
    Textfield Value Should Be       studentID               58660044
TC-PSF-06-06-02 ตรวจสอบการแสดงผลข้อมูลชื่อ-นามสกุล ที่เลือกจากตาราง      
    เปิดหน้าจอ              ${url_manage}
    กดลิ้งค์                 ${pic_follow}
    Textfield Value Should Be       nameStu                 นาย ชรัญธร วงษ์ไทย
TC-PSF-06-06-03 ตรวจสอบการแสดงผลข้อมูลระดับที่เลือกจากตาราง     
    เปิดหน้าจอ              ${url_manage}
    กดลิ้งค์                 ${pic_follow}
    Textfield Value Should Be       stuLevel                ตรี พิเศษ         
TC-PSF-06-06-04 ตรวจสอบการแสดงผลข้อมูลเกรดเฉลี่ยที่เลือกจากตาราง         
    เปิดหน้าจอ              ${url_manage}
    กดลิ้งค์                 ${pic_follow}
    Textfield Value Should Be       stuGpa                  1.94
TC-PSF-06-06-05 ตรวจสอบการแสดงผลข้อมูลสถานภาพที่เลือกจากตาราง    
    เปิดหน้าจอ              ${url_manage}
    กดลิ้งค์                 ${pic_follow}
    Textfield Value Should Be       stuStatus               10
TC-PSF-06-06-06 ตรวจสอบการแสดงผลข้อมูลหน่วยกิตที่ผ่านที่เลือกจากตาราง      
    เปิดหน้าจอ              ${url_manage}
    กดลิ้งค์                 ${pic_follow}
    Textfield Value Should Be       creditPassed            143  
TC-PSF-06-06-07 ตรวจสอบการแสดงผลข้อมูลหน่วยกิตที่ลงที่เลือกจากตาราง      
    เปิดหน้าจอ              ${url_manage}
    กดลิ้งค์                 ${pic_follow}
    Textfield Value Should Be       creditDown              150    
TC-PSF-06-06-08 ตรวจสอบการแสดงผลข้อมูลกล่องกรอกข้อความ
    เปิดหน้าจอ              ${url_manage}
    กดลิ้งค์                 ${pic_follow}
    Page Should Contain Element       stuEnpReg 
TC-PSF-06-06-09 ตรวจสอบการแสดงผลข้อมูลตารางการพูดคุย
    เปิดหน้าจอ              ${url_manage}
    กดลิ้งค์                 ${pic_follow}
    Page Should Contain         ลำดับ
    Page Should Contain         วัน เดือน ปี
    Page Should Contain         แผนการเรียน  



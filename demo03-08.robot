*** Settings ***
Library  SeleniumLibrary
Test Teardown  Close Browser

*** Variables ***
${web_browser}    chrome
#${web_browser}    firefox
#${web_browser}    edge
${url_manage}       http://127.0.0.1:8080/manageStudentConsider
${btnSearch}                    btnsearch
${pic_add}                      xpath=//a[@href="/manageStudentCon_add"]
${pic_edit}                     xpath=//a[@href="/manageCon_edit/1"]
${pic_delete}                   xpath=//a[@href="/delCon/1"]
${txt_studenid}                 studentIDCon
${txt_studnameandlast}          nameStuCon
${nameandlast3}                 ทดสอบ ระบบ
${btnok}                        ok
${btncancel}                    cancel

*** Keywords ***
เปิดหน้าจอ
    [Arguments]             ${url}          
    [Documentation]         กดลิ้งค์เพื่อเปิด Browser
    Open Browser            ${url}             ${web_browser}
กรอกข้อมูล
    [Arguments]             ${txt}             ${input}
    [Documentation]         กรอกข้อมูล
    Input Text              ${txt}             ${input}
กดปุ่ม
    [Arguments]             ${btn}            
    [Documentation]         กดปุ่ม
    Click Button            ${btn}
กดลิ้งค์
    [Arguments]             ${link}            
    [Documentation]         กดลิ้งค์
    Click Link              ${link}
    
*** Test Cases ***
TC-PSF-03-08-01 ตรวจสอบการกดปุ่มบันทึก
    เปิดหน้าจอ        ${url_manage}
    กดลิ้งค์           ${pic_edit} 
    กรอกข้อมูล        ${txt_studnameandlast}         ${nameandlast3}
    กดปุ่ม            ${btnok}
TC-PSF-03-08-02 ตรวจสอบการกดยกเลิก
    เปิดหน้าจอ        ${url_manage}
    กดลิ้งค์           ${pic_edit} 
    กดปุ่ม            ${btncancel} 





*** Settings ***
Library     SeleniumLibrary
Test Teardown   close Browser


*** Variables ***
${web_browser}      chrome
#${web_browser}    firefox
#${web_browser}    edge
${url_manage}       http://127.0.0.1:8080/manageUser
${url_add}          http://127.0.0.1:8080/manageUser_add
${url_edit}         http://127.0.0.1:8080/manageUser_edit/1
${pic_edit}         xpath=//a[@href="/manageUser_edit/1"]
${btnSave}          ok
${btnCancel}        cancel
${txt_username}     username
${status}           statusUser
${txt_pass}       password  
${list1}          อาจารย์

*** Keywords ***
เปิดหน้าจอ
    [Arguments]             ${url}          
    [Documentation]         กดลิ้งค์เพื่อเปิด Browser
    Open Browser            ${url}             ${web_browser}
กรอกข้อมูล
    [Arguments]             ${txt}             ${data_search}
    [Documentation]         กรอกข้อมูล
    Input Text              ${txt}             ${data_search}
กดปุ่ม
    [Arguments]             ${btn}            
    [Documentation]         กดปุ่ม
    Click Button            ${btn}
กดลิ้งค์
    [Arguments]             ${link}            
    [Documentation]         กดลิ้งค์
    Click Link            ${link}
เลือกลิสต์
    [Arguments]                 ${list}
    [Documentation]             เลือกจากในลิสต์
    Select From List By Label   ${status}       ${list}
    
*** Test Cases ***
TC-PSF-02-05-01 ตรวจสอบการกดบันทึก
    เปิดหน้าจอ        ${url_manage}
    เปิดหน้าจอ        ${url_add}
    กรอกข้อมูล        ${txt_username}     test01
    กดปุ่ม            ${btnSave}
TC-PSF-02-05-02 ตรวจสอบการกดปุ่มยกเลิก
    เปิดหน้าจอ        ${url_manage}
    เปิดหน้าจอ        ${url_add}
    กดปุ่ม            ${btnCancel}

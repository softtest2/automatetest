*** Settings ***
Library     SeleniumLibrary
Test Teardown       Close Browser

*** Variables ***
${web_browser}    chrome
#${web_browser}    firefox
#${web_browser}    edge
${url_manage}     http://127.0.0.1:8080/manageUser
${pic_edit}       xpath=//a[@href="/manageUser_edit/1"]
${btn_ok}         ok
${nameuser}       ดร.พิเชษ วะนะลุน
${nameuser1}      2222222222222222222222222222222222222222222222222222
${nameuser2}      
${nameuser3}      er21$5ยน๗xXa^  
${username}       pichet01
${usernameEmpty}  
${username2}      er21$5ยน๗xXa^     
${password}       123
${password1}      1
${passwordEmpty}       
${password2}      er21$5ยน๗xXa^  
${txt_username}   username
${txt_nameuser}   nameuser
${txt_pass}       password

*** Keywords ***
เปิดหน้าจอ
    [Arguments]            ${url}             ${web_browser}
    [Documentation]        กดลิ้งค์เพื่อเปิด Browser
    Open Browser           ${url}             ${web_browser}
กรอกข้อมูล
    [Arguments]            ${txt}             ${input}
    [Documentation]        กรอกข้อมูล
    Input Text             ${txt}             ${input}
กดลิ้งค์
    [Arguments]            ${link}
    [Documentation]        ใช้กดลิ้งค์
    Click link             ${link}
กดปุ่ม
    [Arguments]            ${btn}            
    [Documentation]        กดปุ่ม
    Click Button           ${btn}
*** Test Cases ***
TC-PSF-02-07-01 ตรวจสอบการกรอกข้อมูลชื่อผู้ใช้
        เปิดหน้าจอ                ${url_manage}          ${web_browser}
        กดลิ้งค์                   ${pic_edit}
        กรอกข้อมูล                ${txt_username}        ${username}
TC-PSF-02-07-02 ตรวจสอบการกรอกข้อมูลรหัสผ่าน
        เปิดหน้าจอ                ${url_manage}          ${web_browser}
        กดลิ้งค์                   ${pic_edit}
        กรอกข้อมูล                ${txt_pass}            ${password1}
        กดปุ่ม                    ${btn_ok}
TC-PSF-02-07-03 ตรวจสอบการแสดงผลชื่อ-นามสกุล
        เปิดหน้าจอ                ${url_manage}          ${web_browser}
        กดลิ้งค์                   ${pic_edit}
        กรอกข้อมูล                ${txt_nameuser}        ${nameuser}
TC-PSF-02-07-04 ตรวจสอบการกรอกข้อมูลชื่อผู้ใช้รับไม่เกิน 6 ตัวอักษร และไม่น้อยกว่า 6 ตัวอักษร
        เปิดหน้าจอ                ${url_manage}          ${web_browser}
        กดลิ้งค์                   ${pic_edit}
        กรอกข้อมูล                ${txt_username}        ${username}
TC-PSF-02-07-05 ตรวจสอบการกรอกข้อมูลรหัสผ่านใช้รับไม่เกิน 6 ตัวอักษร และไม่น้อยกว่า 6 ตัวอักษร
        เปิดหน้าจอ                ${url_manage}          ${web_browser}
        กดลิ้งค์                   ${pic_edit}
        กรอกข้อมูล                ${txt_pass}            ${password}
TC-PSF-02-07-06 ตรวจสอบการกรอกข้อมูลชื่อ-นามสกุลรับไม่เกิน 50 ตัวอักษร
        เปิดหน้าจอ                ${url_manage}          ${web_browser}
        กดลิ้งค์                   ${pic_edit}
        กรอกข้อมูล                ${txt_nameuser}        ${nameuser1}
TC-PSF-02-07-07 ตรวจสอบการหากไม่กรอกข้อมูลชื่อผู้ใช้
        เปิดหน้าจอ                ${url_manage}          ${web_browser}
        กดลิ้งค์                   ${pic_edit}
        กรอกข้อมูล                ${txt_username}        ${usernameEmpty}
TC-PSF-02-07-08 ตรวจสอบการหากไม่กรอกข้อมูลรหัสผ่าน
        เปิดหน้าจอ                ${url_manage}          ${web_browser}
        กดลิ้งค์                   ${pic_edit}
        กรอกข้อมูล                ${txt_pass}            ${passwordEmpty}
TC-PSF-02-07-09 ตรวจสอบการหากไม่กรอกข้อมูลชื่อ-นามสกุล
        เปิดหน้าจอ                ${url_manage}          ${web_browser}
        กดลิ้งค์                   ${pic_edit}
        กรอกข้อมูล                ${txt_nameuser}        ${nameuser2}
TC-PSF-02-07-10 ตรวจสอบการหากกรอกข้อมูลชื่อผู้ใช้เป็นอักษรมั่ว
        เปิดหน้าจอ                ${url_manage}          ${web_browser}
        กดลิ้งค์                   ${pic_edit}
        กรอกข้อมูล                ${txt_username}        ${username2}
TC-PSF-02-07-11 ตรวจสอบการหากกรอกข้อมูลรหัสผ่านเป็นอักษรมั่ว
        เปิดหน้าจอ                ${url_manage}          ${web_browser}
        กดลิ้งค์                   ${pic_edit}
        กรอกข้อมูล                ${txt_pass}            ${password2}
TC-PSF-02-07-12 ตรวจสอบการหากกรอกข้อมูลชื่อ-นามสกุลเป็นอักษรมั่ว
        เปิดหน้าจอ                ${url_manage}          ${web_browser}
        กดลิ้งค์                   ${pic_edit}
        กรอกข้อมูล                ${txt_nameuser}        ${nameuser3}

